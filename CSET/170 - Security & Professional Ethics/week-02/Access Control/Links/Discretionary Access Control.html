<html>

<head>
<link rel="stylesheet" type="text/css" href="notes.css">
<title>Discretionary Access Control</title>
</head>

<body class=notes> 

<h1>Discretionary Access Control</h1>

<p>Recall that Lampson's <i> gold standard</i> identifies authorization, authentication,
and audit as essential mechanisms for computer security. We begin studying
authorization, which controls whether actions of principals are allowed, by
considering access control. An <i>access control policy</i> specifies
access rights, which regulate whether requests made by principals should be
permitted or denied.</p>

<p>In access control, we refine the notion of a principal to be one of a:</p>
<ul>
  <li><i>user: </i>a human</li>
  <li><i>subject: </i>a process executing on behalf of a user</li>
  <li><i>object: </i>a piece of data or a resource.</li>
</ul>

<p>Why is that distinction important?
Executing programs can be controlled; people can't. People can have different 
identities that they convey to programs.  And the same program can be executed by 
different users.  Determining which user a subject corresponds to is the 
problem of authentication, which we've already covered.</p>

<p>The basic model we have in mind is that a subject attempts to access an object.
The object is protected by a guard called a <i>reference monitor</i>.
The principle of Complete Mediation says that reference monitor must check every access.</p>

<p>The primary concerns of an access control system are the following:</p>
<ul>
<li><b>Prevent access:</b> in the absence of any privilege, 
    ensure that the subject cannot access the object.
    The principle of Failsafe Defaults says that this should be the default.</li>
<li><b>Determine access:</b> decide whether a subject has access, according
    to some policy, to take an action with an object.</li>
<li><b>Grant access:</b>  give a subject access to an object.  The principle of Separation 
    of Privilege says this should be fine-grained; don't grant
    access to many objects just to enable access to one.</li>
<li><b>Revoke access:</b>  remove a subject's access to an object.</li>
<li><b>Audit access:</b>  Determine which subjects can access
    an object, or which objects a subject can access.</li>
</ul>

<p>A <i>discretionary access control</i> (DAC) policy is a means of assigning access
rights based on rules specified by users. The underlying philosophy in DAC is that
subjects can determine who has access to their objects.</p>

<p>DAC policies includes
the file permissions model implemented by nearly all operating systems. In
Unix, for example, a directory listing might yield "... <tt>rwxr-xr-x ...
file.txt</tt>", meaning that the owner of file.txt may read, write, or execute
it, and that other users may read or execute the file but not write it.
The set of access rights in this example is {read, write, execute}, and the
operating system mediates all requests to perform any of these actions.
Users may change the permissions on files they own, making this a discretionary
policy.</p>

<p>A mechanism implementing a DAC policy must be able to answer the
question: "Does subject S have right R for object O?"
Abstractly, the information needed to answer this question can be
represented as a mathematical relation D on subjects, objects, and
rights: if (S,O,R) is in D, then S does have right R for object O;
otherwise, S does not. More practically, the same information could also
be represented as an <i>access control matrix</i> [Lampson 1971]. Each row of the
matrix corresponds to a subject and each column to an object. Each cell of
the matrix contains a set of rights. For example:</p>

<div align="center">
  <table border="1">
    <tr>
      <td></td>
      <td>file1</td>
      <td>file2</td>
    </tr>
    <tr>
      <td>Alice</td>
      <td>rwx</td>
      <td>r-x</td>
    </tr>
    <tr>
      <td>Bob</td>
      <td>r--</td>
      <td>rw-</td>
    </tr>
  </table>
</div>
  
<p>Real systems typically store the information from this matrix either by
columns or by rows.</p>

<p>An implementation that stores by columns is commonly known as
an <i>access control list</i> (ACL). File systems in Windows and Unix
typically use such an implementation: each file is accompanied by a list
of entries (s, rs), 
containing subjects s and their rights rs to that file. An implementation that
stores by rows is commonly known as a <i>privilege list</i> or a 
<i>capability list</i>. Each subject maintains an
unforgeable list of entries (o, rs) containing objects o and rights rs to that object.
Android uses a privilege list: each app can be granted the right to access the 
network, GPS, phone, etc.</p>

<p>Each implementation makes certain auditing concerns easier to address than 
others:</p>
<ul>
<li>With ACLs, it's hard to audit what objects a subject can access, but 
easy to audit which subjects can access an object.</li>
<li>With privilege lists, it's
easy to audit what objects a subject can access,
but it's hard to audit which subjects can access an object,
unless the system maintains a global map of capabilities.</li>
</ul>

<h2>Cryptographic Capabilities</h2>

<p>The discussion of privilege/capability lists above suggested that a trusted 
access control system manage storage of the lists.  In a distributed system,  
it would instead be possible to have untrusted subjects manage the storage
of those lists.  Alice could keep track of the capabilities issued to her,
Bob of those to him, and so forth.  But now the <i>authenticity</i> of
those capabilities must be ensured:  we would not want subjects to be
able to manufacture capabilities never issued to them by the access control system.
</p>

<p>Digital signatures provide a mechanism to implement capabilities 
in this distributed setting.  To issue a right <tt>r</tt> to a subject 
named <tt>S</tt> for an object named <tt>O</tt>,
the access control system can construct a capability <tt>cap</tt>, where 
<tt>cap=(S,O,r)</tt>, and sign that capability, producing a signature <tt>s</tt>,
where <tt>s=Sign(cap; k_AC)</tt> and where <tt>k_AC</tt> is a private signing
key for the access control system. The pair <tt>(cap,s)</tt> can be given by
the access control system to the subject.  Whenever the subject wishes access,
it presents <tt>(cap,s)</tt> back to the access control system, which
verifies the signature and the rights contained in the capability. </p>

<p>This scheme can be modified support delegation:</p>
<ul>
<li>The subject's name could be omitted from the capability.  This would enable
subjects to easily delegate capabilities, simply by giving another subject
a copy of the capability.</li>
<li>Instead of using a single key pair, many key pairs could be employed.  For
example, there could be one key pair <tt>(K_O, k_O)</tt> per object <tt>O</tt>
in the system.  The access 
control system could delegate management of access control for <tt>O</tt>
to a subject <tt>S</tt> by giving <tt>k_O</tt> to <tt>S</tt>.
Any component that needed to verify
capabilities for <tt>O</tt> would need to know <tt>K_O</tt>. 
</ul>

<p>Revocation, however, now becomes a problem, much like it did for
digital certificates.  It might not be possible to delete capabilities
once they have been issued.  So the access control system might need
to include validity intervals (so that capabilities expire), or maintain
revocation lists (which must be consulted by the reference monitor).</p>

<p>Moreover, these capabilities are expensive.  Creating keys and verifying
digital signatures both take time, and storing the signature takes space.</tt>

<h2>Commands</h2>

<p>The policy enforced by an access control system is rarely static.
<i>Commands</i> are issued to change the policy.  An access control
system might provide commands to create and destroy objects, grant and revoke
rights to objects, create and destroy subjects, create trust relationships
between subjects, etc.  A command can be specified as follows:</p>

<pre>
command C(args)
  requires: R
  ensures: E
</pre>

<p><tt>C</tt> is the name of the command, <tt>R</tt> is a precondition
that must hold for the command to be executed, and <tt>E</tt> is
the effect the command has on the access control system.</p>

<p>For example, the access control system for a file system might include
include a right called <tt>own</tt> that indicates a subject owns an object.
And this system might allow the owner of a file to grant the right to read
the file to any other subject.  That would be realized by
the following command:</p>

<pre>
(* subject s grants read right for file f to subject q *)
command grant-read-file(s,f,q)
  requires: (s,f,own) \in D
  ensures: D := D \union {(q,f,read)}
</pre>

<p>As another example, the system might support a command to create files,
which grants the creator the <tt>own</tt>, <tt>read</tt>, and <tt>write</tt>
commands:</p>

<pre>
(* subject s creates file f *)
command create-file(s,f)
  requires: f does not already exist as an object in D
  ensures: D := D \union {(s,f,own), (s,f,read), (s,f,write)}
</pre>

<p>Some access control systems permit subjects other than the owner
to <i>delegate</i> their rights to an object to another subject.
For example, Alice might delegate her rights to read file <tt>f</tt>
to Bob.  The right to delegate rights to other principals is sometimes
called the <i>copy right</i> or the <i>grant right</i>.  A fine-grained
access control system will associate a distinct copy right with every
right.  For example, the <tt>read</tt> right could be associated with
the <tt>delegate-read</tt> right, the <tt>execute</tt> right could be associated with
the <tt>delegate-execute</tt> right, etc.</p>

<p>Here is a revised command for grant the right to read a file:</p>
<pre>
(* subject s delegates read right for file f to subject q *)
command grant-read-file(s,f,q)
  requires: (s,f,delegate-read) \in D
  ensures: D := D \union {(q,f,read)}
</pre>
<p>Note that <tt>q</tt> will not be able to further delegate the <tt>read</tt>
right to other principals, because the command does not grant <tt>delegate-read</tt>
to <tt>q</tt>.</p>


<h2>Groups and Roles</h2>

<h3>Groups</h3>

<p>Students get access to a class because they are enrolled in it,
not because of their own individual identities.  Faculty get access
to a fancy dining room on campus [would that were true...]
because of their job status, not 
because of their own individual identities.  
In both cases, access is determined by being a member of a <i>group</i>.
</p>

<p>Group membership changes over time.  So it's not wise to assign
group-determined rights directly to individuals, because then administrators 
would need to redetermine rights anytime group memberships change.
It's better to assign rights to a new kind of subject,
a group, which is a named set of subjects.</p>

<p>In ACLs, we can have a new kind of entry (g, rs), where g is group name.
And separately, we can have a group list with entries (g, subjs),
where g is the group name and subjs is set of subjects.  
For capabilities, the implementation becomes a little more complex;
we omit details here.</p>

<h3>Role-based Access Control</h3>

<p>In the real world, security policies are dynamic. Access rights,
whether discretionary or mandatory, need to change as the responsibilities of
users change. This can make management of rights difficult. When a
new user is authorized for a system, the appropriate rights for that user must
be established. When a user changes job functions, some rights should be
deleted, some maintained, and some added. </p>

<p>Role-based access control (RBAC) addresses this problem by changing the
underlying subject&ndash;object model. A <i>role</i> is a job function or
title&mdash;i.e., a set of actions and responsibilities associated with a particular
working activity. Now, instead of an access control policy being a
relation on subjects, objects, and rights, a policy is a relation on roles,
objects, and rights; this is called a <i>right assignment</i>. For
example, the role "5430 TA" might be assigned the right to grade 5430
homeworks. Further, subjects are now assigned to roles; this is called a
<i>role assignment</i>. Each subject may be assigned to many roles, and
each role may be assigned to many subjects. Finally, roles are
hierarchical. For example, the role "5430 Professor" should have
all the rights that a "5430 TA" does, and more. </p>

<p>Roles are similar to groups in Unix file system DAC, with two important
distinctions. First, a group is a set of users, whereas a role is a set of
rights. Second, at least in some implementations, 
a user is always a member of a group, whereas a subject may
activate or deactivate the rights associated with any of the subject's
roles. This enables finer-grained implementation of the Principle of Least
Privilege. Subjects may login with most of their roles deactivated, and
activate a role only when the rights associated with the role are necessary.</p>

</body>

</html>