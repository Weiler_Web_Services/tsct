# Exercise 48: test_lexicon.py
# NOTE: this should be added to the tests directory inside a newly created project
from ex48 import lexicon
def test_directions():
    assert lexicon
    assert lexicon.scan("north") == [('direction', 'north')]
    result = lexicon.scan("north south east")
    assert result == [
            ('direction', 'north'),
            ('direction', 'south'),
            ('direction', 'east')]
def test_verbs():
    assert lexicon.scan("go") == [('verb', 'go')]
    result = lexicon.scan("go kill eat")
    assert result == [
            ('verb', 'go'),
            ('verb', 'kill'),
            ('verb', 'eat')]
 
