Python 3 


The // operator performs integer (floor) division and the / operator performs float (true) division. An example for floor division is 50 // 11 = 4. An example for true division is 50 / 11 = 4.545454545454546.

Note that floor division always rounds ''down'', i.e., 3 // 2 == 1 and -3 // 2 == -2.
-------------------------------------------

for i in range(3):
	print(i) 
0
1
2
-------------------------------------------
print(sum(range(0,7)))
21

The range function returns a sequence starting from the first value (inclusive) and ending at the second value (exclusive).
-------------------------------------------
a = 'hello'
b = 'world'
print(a, b, sep=' Python ', end='!')
hello Python world!

The print function has several parameters which you can use to format the output.
The parameter 'sep' indicates the separator which is printed between the objects. By default 'sep' is empty space. In the puzzle we set it to be ' Python '. The parameter 'end' defines what comes at the end of each line. By default 'end' is a line break. In the puzzle we set it to '!'. This means that print would print everything in one single line because there is no line break.
When we call print() with the given parameters and objects a and b we get the output 'hello Python world!'
-------------------------------------------
text = ('What is the answer?'
	' 42!')
print(text)
What is the answer? 42!
This feature is particularly useful when you want to break long strings.
-------------------------------------------
print('\"hello' + " "
	+ "world's end\"")
"hello world's end"
"Besides numbers, Python can also manipulate strings, which can be expressed in several ways. They can be enclosed in single quotes ('...') or double quotes ("...") with the same result [2]. \ can be used to escape quotes."
-------------------------------------------
print(x[2] + x[1] + x[0]
	+ x[5] + x[3] + x[4])
listen
In Python, you can access every character in the string by using an integer value that defines the position of the character in the string. We call this integer value an index.

If the string has five characters as in the example, the indices of these characters are as follows.

s i l e n t

0 1 2 3 4 5

You can index any character using the square bracket notation [] with their respective position values. Many programming novices are confused by the fact that the first element in a sequence has index 0. Therefore, you access the first character 's' with the expression s[0] and the third character 'l' with the expression s[2].

The plus operator + is context sensitive. It calculates the mathematical sum for two given numerical values but appends strings for two given string values. For example, the expression 'a' + 'b' returns a new string 'ab'.

With this information, you are now able to determine how string s is reordered using indexing notation and the + operator for strings.

A small note in case you were confused. There is no separate character type in Python; a character is a string of size one.
-------------------------------------------
print('P"yt\'h"on')
P"yt'h"on
This puzzle introduces several Python language features about quotes in string literals. It requires a clear understanding of the concept of escaping. Escaping is an important concept in most programming languages. You are not an advanced coder without understanding at least the basic idea of escaping.

Strings can be enclosed either with the single quotes '...' or double quotes "...". These two options are semantically equivalent, i.e., they do the same thing.

But what happens if you write, say, a small conversation with direct speech?

"Alice said: "Hey Bob!" and went on." (wrong)

The double quotes cannot be a part of a string enclosed in double quotes. Trying this ends the string prematurely. Here, the best case is that the interpreter complains about the strange syntax of the random character sequence after the premature ending of your string.

Yet, there is an easy fix. You can avoid this problem by enclosing the string with single quotes:

'Alice said: "Hey Bob!" and went on.' (right)

The double quotes can now be part of the string itself without ending the string sequence. The opposite also works, i.e., writing a single quote within a string enclosed in double quotes.

So far so good. But there is still one question left that is also the main reason why only 25% of Finxters can solve this puzzle: escaping. What if we want to put a single quote within a string enclosed by single quotes?

In the puzzle, we solve this using the escape character: the backslash. When put before special characters like the single quote, it escapes them. In other words, it changes the meaning of these characters. For example, the single quote has the meaning of starting or ending a string. But when escaped, the meaning of the single quote character is just the single quote.
-------------------------------------------


-------------------------------------------


-------------------------------------------


-------------------------------------------


-------------------------------------------


-------------------------------------------


-------------------------------------------


-------------------------------------------


-------------------------------------------
