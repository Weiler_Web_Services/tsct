# for statement
print "'for' example:"
for x in range(5):
    print x,

# for with "break"
print "\n\n'for' example using 'break':"
for x in range(5):
    print x,
    if x==2:
       break

# for with "continue"
print "\n\n'for' example using 'continue':"
for x in range(5):
    if x==2:
       continue
    print x,